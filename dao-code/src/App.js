import React, { Component } from "react";
import axios from "axios";
import "./App.css";
import { API_KEY } from "./config";
import { hasGeolocationAccess } from "./helpers";
import TodayCard from "./components/TodayCard/TodayCard";
import Input from "./components/Input/Input";
import Button from "./components/Button/Button";

class App extends Component {
  state = {
    weatherData: null,
    geoAccess: null,
    inputValue:''

  };
  componentDidMount = () => {
    if (hasGeolocationAccess) {
      this.getWeather();
    }
   
  };
  getInputValue=(value)=>{
this.setState({inputValue:value})
  }

  getWeather = () => {
    if(this.state.inputValue){
      axios
        .get(
          `https://api.openweathermap.org/data/2.5/weather?q=${this.state.inputValue}&appid=${API_KEY}&units=metric&lang=ru`
        )
        .then((res) => {
          this.setState({ weatherData: res.data, geoAccess: true });
          this.getWeaterColor()
        });
    }else
    navigator.geolocation.getCurrentPosition((pos) => {
      this.coords = {
        longitude: pos.coords.longitude,
        latitude: pos.coords.latitude
      };
      axios
        .get(
          `https://api.openweathermap.org/data/2.5/weather?lat=${this.coords.latitude}&lon=${this.coords.longitude}&appid=${API_KEY}&units=metric&lang=ru`
        )
        .then((res) => {
          this.setState({ weatherData: res.data, geoAccess: true });
          this.getWeaterColor()
        });
    });
  };
  getWeaterColor=()=>{
    const{weatherData}=this.state
    let temp = weatherData.main.temp
    let  background=''
    if(temp <= -10){
      background='#00FFFF'
    }
    else if(temp > -10&& temp <10){
      background='#f7ffd4'
    }
    else if(temp >=10&&temp<30){
      background='#FFF700'
    }
    else if(temp>=30){
      background='#FF8C00'
    }
return document.body.style.background = background

  }
  render() {
    const { geoAccess, weatherData, inputValue } = this.state;
    
    if (geoAccess && weatherData) {
      return (
        <div className="App app-wrapper">
          <form onSubmit={(e)=>e.preventDefault(this.getWeather)} className="input-wrapper">
            <Input className='weather-input' type='search' value={inputValue} placeholder="Enter your city" func={this.getInputValue}/>
            <Button className="weather-button" text="Search" func={this.getWeather}/>
          </form>
          <TodayCard
            emoji={weatherData.weather[0]}
            main={weatherData.main}
            city={weatherData.name}
            country={weatherData.sys.country}
          />
          
        </div>
      );
    }return(
      <div className="App"></div>
    )
  }
}

export default App;
