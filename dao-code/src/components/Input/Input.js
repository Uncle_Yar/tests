import React from "react";


const Input = (props) => {
  const {className, type, value, placeholder} = props;
  const inputChange = (e) => {
    props.func(e.target.value);
  };
  return (
    <input className={className} type={type} onChange={inputChange} value={value}
    placeholder={placeholder}></input>
  );
};

export default Input;