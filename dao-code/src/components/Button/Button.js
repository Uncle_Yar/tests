import React from "react";

const Button = (props)=>{
    const { className, text, func } = props;
    return (
      <button className={className} onClick={func}>
        {text}
      </button>
    );
  
}

export default Button;
