import React from 'react';
import './Line.scss'

function Line(props) {
  
  return (
    <div style={{top:props.top +'px',left:props.left +'px'}} className={props.className}>
    </div>
  );
}

export default Line;