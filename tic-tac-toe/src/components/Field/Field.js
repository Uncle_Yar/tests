import React, { Component } from "react";
import Line from "../Line/Line";
import Square from "../Square/Square";
import { setLine } from "../../helpers/helpers";
import "./Field.scss";

export default class Field extends Component {
  showSquare(i) {
    return (
      <Square
        id={i}
        value={this.props.squares[i]}
        func={() => this.props.func(i)}
      />
    );
  }
  render() {
    let exactLine = setLine(this.props.lines);
    return (
      <div>
        {exactLine && (
          <Line
            top={exactLine[0].top}
            left={exactLine[1].left}
            className={exactLine[2].className}
          />
        )}
        <div className="field-row">
          {this.showSquare(0)}
          {this.showSquare(1)}
          {this.showSquare(2)}
        </div>
        <div className="field-row">
          {this.showSquare(3)}
          {this.showSquare(4)}
          {this.showSquare(5)}
        </div>
        <div className="field-row">
          {this.showSquare(6)}
          {this.showSquare(7)}
          {this.showSquare(8)}
        </div>
      </div>
    );
  }
}
