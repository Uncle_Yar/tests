import React from 'react';
import './Square.scss'

function Square(props) {
  return (
    <div className='square'>
      
    <button id={`square ${props.id}`} className='square-btn' onClick={props.func}>
{props.value}
    </button>
    </div>
  );
}

export default Square;