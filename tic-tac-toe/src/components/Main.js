import React, { Component } from "react";
import Field from "./Field/Field";
import Modal from "./Modal/Modal";
import { isWinner } from "../helpers/helpers";
import "./Main.scss";

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNextX: true,
      stepNumber: 0,
      history: [{ squares: Array(9).fill("") }],
      playerOne: { name: "noname", scores: 0 },
      playerTwo: { name: "noname", scores: 0 },
      isModal: true,
    };
  }

  reSeter(winner, current) {
    if (winner === "X") {
      setTimeout(()=>this.setState({
        ...this.state,
        isNextX: 0 % 2 === 0,
        stepNumber: 0,
        playerOne: {
          ...this.state.playerOne,
          scores: this.state.playerOne.scores +0.5,
        },
      }),2000)
    } else if (winner === "O") {
      setTimeout(()=>this.setState({
        ...this.state,
        isNextX: 0 % 2 === 0,
        stepNumber: 0,
        playerTwo: {
          ...this.state.playerTwo,
          scores: this.state.playerTwo.scores +0.5,
        },
      }),2000)
      
    } else if (!current.squares.includes("") && !winner) {
      this.setState({
        ...this.state,
        isNextX: 0 % 2 === 0,
        stepNumber: 0,
      });
    }
  }
  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    const winnerInfo = isWinner(squares);
    const winner = winnerInfo.winner;
    if (winner || squares[i]) {
      return;
    }
    squares[i] = this.state.isNextX ? "X" : "O";
    this.setState({
      history: history.concat({
        squares: squares,
      }),
      isNextX: !this.state.isNextX,
      stepNumber: history.length,
    });
  }
  inputHandle = (value, id) => {
    if (id === "firstPlayer") {
      this.setState({
        ...this.state,
        playerOne: { ...this.state.playerOne, name: value },
      });
    } else if (id === "secondPlayer")
      this.setState({
        ...this.state,
        playerTwo: { ...this.state.playerTwo, name: value },
      });
  };
  closeModal = () => {
    this.setState({
      ...this.state,
      isModal: !this.state.isModal,
    });
  };

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winnerInfo = isWinner(current.squares);
    const winner = winnerInfo.winner;
    const lines = winnerInfo.lines;
    let renderWin;
    if (winner === "X" && winner !== null) {
      renderWin = `${this.state.playerOne.name} is won`;
      this.reSeter(winner,current)
    } else if (winner === "O") {
      renderWin = `${this.state.playerTwo.name} is won`;
      this.reSeter(winner,current)
    } else if (!current.squares.includes("") && !winner) {
      this.reSeter(winner,current)
      renderWin = "DRAW!";
    }

    return (
      <div>
        {this.state.isModal ? (
          <Modal
            className="main-modal"
            func={this.inputHandle}
            closeModal={this.closeModal}
            text={"Please enter your names!"}
          />
        ) : (
          <div className="main">
            <div className="main-field">
              <Field
                func={(i) => this.handleClick(i)}
                squares={current.squares}
                lines={lines}
              />
            </div>
            <div className="main-info">
              <p>Scores:</p>
              <p>
                {this.state.playerOne.name}: {this.state.playerOne.scores}
              </p>
              <p>
                {this.state.playerTwo.name}: {this.state.playerTwo.scores}
              </p>
              <div>{renderWin}</div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
